#! /usr/bin/python
#! encoding: utf8

__author__ = 'kirienko'


from d_to_infty_class_ext_mom import D_to_infty_graph as D
from sympy import var, symbols,  factor, ln, prod, simplify, fraction, factor_list, pi, gamma, Rational
from itertools import product
from copy import deepcopy
from sign_account import sign_account
from collections import defaultdict
import os, sys
from functools import reduce


def cut_edges(G,sub1,sub2):
    """
    :param G: initial networkx graph
    :param sub1: list of nodes of the subgraph 1 of G
    :param sub2: list of nodes of the subgraph 1 of G
    :return: list of the internal edges of G such that are cut
    """
    edges = list(G.subgraph(sub1).edges(keys=True)) + list(G.subgraph(sub2).edges(keys=True))
    return [e for e in G.edges(keys=True) if (e not in edges and -1 not in e and -2 not in e)]


def stretches_to_exponent(expr, stretch_vars):
    """
    :param expr: sympy выражение
    :param stretch_vars: список sympy-переменных параметров растяжений, которые есть в expr
    :return: выражение, где все растяжения из множителей перешли в показатели
    """
    # Возвращает число слагаемых, при которых множителем стоит параметр растяжения stretch_factor
    def number_of_terms(expr, stretch_factor):
        s = expr.coeff(stretch_factor, 1)  # сумма при множителе stretch_factor
        number = str(s).count('+')
        if number > 0:
            number += 1
        elif number == 0 and str(s) != '0':
            number += 1
        return number

    stretch_degrees = dict(list(zip(stretch_vars, [number_of_terms(expr, st) for st in stretch_vars])))

    expr_list = []
    aux_expr = expr.copy()
    while len(stretch_degrees) != 0:
        stretch = max(stretch_degrees, key=stretch_degrees.get)
        if stretch_degrees[stretch] != 0:
            expr_list.append((stretch, aux_expr.coeff(stretch, 1), aux_expr.coeff(stretch, 0)))
            aux_expr = aux_expr.coeff(stretch, 1)
        stretch_degrees.pop(stretch)

    final_expr = 0
    for i in expr_list[::-1]:
        if final_expr == 0:
            final_expr = i[1]
        final_expr = pow(final_expr, i[0]) + i[2]
    #ЛЮТЫЙ КОСТЫЛЬ
    #!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    if final_expr==0:
        return expr
    # !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    return final_expr


def integrand_with_ext_mom(graph_object, time_ver_number):
    """
    Returns integrand ( = numerator/denominator) that corresponds certain time version tv.
    No integral sign, no differentiantion and integration of stretching parameters.
    """
    version = graph_object.tv[time_ver_number]
    v = version[0]   # последовательность вершин из вр версии
    var(['k%d'%j for j in range(graph_object.Loops)])   # определяем импульсы интегрирования как sympy переменные
    var('p')
    var(['a%d'%j for j in range(graph_object.Loops-1)])
    sq = lambda x: x**2
    stretch_vars_all = set()  #все параметры растяжения, которые присутствуют в вр версии (заполняем по ходу цикла по сечениям) - понадобится для вычитаний
    denominator = 1
    den_list = [] #список множителей в знаменателе
    for l in range(len(v)-1):  # цикл по временным сечениям
        all_momenta = [var('k%i'%i) for i in range(graph_object.Loops)]   # список переменных интегрирования
        all_momenta.append(var('p'))
        stretch_factors = dict(list(zip(all_momenta, [1 for i in range(len(all_momenta))])))  # 4loop пример: {k0: 1, k1: 1, k2: 1, k3: 1}
        edges = cut_edges(graph_object.U, v[:l+1], v[l+1:])  # линии временного сечения
        # Check if edges are in some significant subgraph
        stretch_vars = set() #список параметров растяжения
        for j,sub in enumerate(graph_object.tv[time_ver_number][1]):  # цикл по расх подграфам во вр версии
            sg_nodes = [n for n in sub.vertices if n>-1]  #список вершин по Никелю, которые входят в расх подграф вр версии
            sg = graph_object.U.subgraph(sg_nodes)  #подграф по вершинам в networkx
            ## If the cut shares edges with subgraph, do stretches
            if set(sg.edges(keys=True)).intersection(set(edges)): #если линии расх подграфа пересекаются сечением
                ## Find simple momenta in this subgraph
                sg_simple_mom = [graph_object.momenta_in_edge_with_ext_momentum(e)[0] for e in sg.edges(keys=True) if len(graph_object.momenta_in_edge_with_ext_momentum(e)) == 1]
                for k in stretch_factors:
                    if str(k) not in sg_simple_mom:
                        stretch_factors[k] *= var('a%d'%j)
                        stretch_vars.add((var('a%d'%j), sub)) #!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        den = []

        for e in edges:  # цикл по линиям в сечении
            m = [var(x) for x in graph_object.momenta_in_edge_with_ext_momentum(e)]
            #den += factor(list(map(sq,[stretch_factors[x]*x for x in m])))
            den += factor(list([stretch_factors[x] * sq(x) for x in m]))

        #Далее поднимем все параметры растяжения в показатели степени.
        if len(stretch_vars)!=0:
            final_expr = stretches_to_exponent(sum(den), [v[0] for v in stretch_vars]) #!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            # Хотим, чтобы одинаковые множители в знаменателе не повторялись в списке,
            # а увеличивали степень исходного множителя:
            '''
            if final_expr in den_list:
                den_list[den_list.index(final_expr)] *= final_expr
            else:
                den_list.append(final_expr)
            '''
            den_list.append(final_expr)

            denominator *= final_expr
            stretch_vars_all = stretch_vars_all.union(stretch_vars) # добавляем растяжения из данного сечения в общий список
        else:
            den_factor = sum(den)
            denominator *= den_factor
            # Хотим, чтобы одинаковые множители в знаменателе не повторялись в списке,
            # а увеличивали степень исходного множителя:
            '''
            if den_factor in den_list:
                den_list[den_list.index(den_factor)] *= den_factor
            else:
                den_list.append(den_factor)
            '''
            den_list.append(den_factor)

    numerator = 1
    num_list = [] #список множителей в числителей
    #numerator *= reduce(lambda x,y: x*y, var(['k%d'%j for j in range(graph_object.Loops)])) # перемножение всех простых моментов в числителе
    for b in graph_object.bridges:
        test = [int(graph_object.flow_near_node(x),2) for x in b]
        shared_mom_bin = bin(test[0]&test[1])[2:]
        shared_mom = [var("k%d"%j) for j,k in enumerate(shared_mom_bin[::-1]) if int(k) and j < graph_object.Loops]
        if len(shared_mom) == 0:
            continue
        else:
            # Список вершин, формирующих хребет
            spine_list = set()
            for edge in graph_object.spine:
                spine_list.update(edge.nodes)

            #Проверяем, входят ли оба конца рельсов в хребет.
            #Если нет, то внешнего импульса в рельсах не будет, и работает старый код.
            if b[0] in spine_list and b[1] in spine_list:
                shared_mom.append(var('p'))

            num = 0
            stretch_vars_bridges = set() #cписок параметров растяжения в рельсах
            for m in shared_mom:
                term = sq(m)
                for j, sub in enumerate(graph_object.tv[time_ver_number][1]):
                    ext_nodes = set([e.nodes[1] for e in sub.external_edges])
                    all_nodes = set([x for x in sub.vertices if x>-1])
                    int_nodes = all_nodes.difference(ext_nodes)
                    internal_mom = graph_object.subgraph_simple_momenta(sub)

                    #Возведение растяжения в квадрат - вроде не играет значения
                    if str(m) not in internal_mom and b[0] in int_nodes and b[1] in int_nodes:
                        term *= var('a%d'%j)
                        stretch_vars_bridges.add(var('a%d'%j))
                    elif str(m) not in internal_mom and b[0] in int_nodes and b[1] not in int_nodes:
                        term *= var('a%d'%j)
                        # stretch_vars_bridges.add(var('a%d' % j))   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                        print('ВНИМАНИЕ! СРАБОТАЛО СТРАННОЕ УСЛОВИЕ 2 В 82 СТРОКЕ get_integrands_ext_mom')
                    elif str(m) not in internal_mom and b[0] not in int_nodes and b[1] in int_nodes:
                        term *= var('a%d'%j)
                        # stretch_vars_bridges.add(var('a%d' % j))   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                        print('ВНИМАНИЕ! СРАБОТАЛО СТРАННОЕ УСЛОВИЕ 3 В 82 СТРОКЕ get_integrands_ext_mom')
                num += term
            if len(stretch_vars_bridges)!=0:
                final_expr = stretches_to_exponent(num, stretch_vars_bridges)
                numerator *= final_expr
                num_list.append(final_expr)
            else:
                numerator *= (num)
                num_list.append(num)

    # Возвращает список множителей в числителе(num_list) и полное выражение в знаменателе (denominator)
    # Так надо для Фейнмана
    print('num_list: ', num_list)
    print('den_list: ', den_list)
    return num_list, den_list, list(stretch_vars_all), graph_object

def substration(num_list, den_list, stretch_vars, graph_object):
    """
    :param num_list: список множителей в числителе с растяжениями
    :param den_list: список множителей в знаменателе с растяжениями
    :param stretch_vars: множество пар (переменная_растяжения, номенклатура соотв. ей подграфа)
    :return: список интегрлов после подстановки [[[список множителей в числителе],[список множителей в знаменателе], знак перед интегралом(1 или -1)], ...]
    """
    perms = list(product([0, 1], repeat=len(stretch_vars)))
    sympy_subs = []

    #Знак от протечки импульсов в диаграмме
    sign_from_mom_flow = sign_account(graph_object)

    for perm in perms:
        sub = list(zip([v[0] for v in stretch_vars], perm))
        sub = list(map(list, sub))
        if perm.count(0)%2 == 0:
            sign = 1
        else:
            sign = -1
        sympy_subs.append([sub, sign*sign_from_mom_flow])

    subs_list = []
    for sub in sympy_subs:
        subs_list.append([list(map(lambda x: x.subs(sub[0]), num_list)),
                          list(map(lambda x: x.subs(sub[0]), den_list)),
                          sub[1]])
    print(subs_list)
    return subs_list


def sympy_to_maple(sp_expr):
    """
    sympy выражение переводит в мэпловское
    :param sp_expr: sympy выражение
    :return: maple_expr: мэпловская команда
    """
    maple_expr = str(sp_expr)
    maple_expr = maple_expr.replace("gamma", "GAMMA")
    return maple_expr


def feyn_repr(integrand):
    """
    фейнмановское представление с учетом множителей в числителе
    :param integrand: [[список множителей в числителе],[список множителей в знаменателе], знак перед интегралом(1 или -1), число активных петель] - подинтегральное выражение (подстановка растяжек 0 1 уже сделана)
    :return: param_int - подинтергральное выражение, которое пойдет в hyper_int
    :return: non_int_expr - внеинтегральный множитель
    :return: diff_vars - список переменных, соотв множителям из числителя, по которым будем дифф в мэпле
    """
    # ВАЖНО: эта функция работает только если все множители и в числителе, и в знаменателе имеют первую степень!
    n = len(integrand[1])
    m = len(integrand[0])
    sign = integrand[2]
    print('ЗНАК: ', sign)

    all_factors = integrand[1] + integrand[0]

    u_list = [var('u%d'%(i + 1)) for i in range(n + m)]
    quad_form = 0
    var_list = set()

    for i in range(m + n):
        var_list = var_list.union(all_factors[i].free_symbols)
        quad_form += u_list[i] * all_factors[i]
    var_list.remove(p)
    quad_form = quad_form.factor()

    det = 1
    c = quad_form.copy()
    for v in var_list:
        mom_coeff = quad_form.coeff(v**2)
        det *= mom_coeff
        c -= mom_coeff*v**2
    c = simplify(c)

    d, E = symbols('d E')
    l = len(var_list)  # число петель
    alpha = n - m
    param_int = det**(-d/2) * c**(-(n-m)+d*l/2)

    #diff_vars - список переменных, соотв множителям из числителя, по которым будем дифф в мэпле
    diff_vars = str([var('u%d'%(n + i + 1)) for i in range(m)])

    #param_int = simplify(param_int.subs([(d, 2-2*E)])) #подинтергральное выражение, которое пойдет в hyper_int
    param_int = param_int.subs([(d, 2 - 2 * E)])  # подинтергральное выражение, которое пойдет в hyper_int
    non_int_expr = (sign * Rational(2**(-2*l)) * gamma(alpha-d*l/2) * (gamma(d/2)) ** (l)).subs(d,2-2*E)  # внеинтегральный множитель
    return sympy_to_maple(param_int), sympy_to_maple(non_int_expr), diff_vars

#СТАРОЕ
def get_letters_k(eq,L):
    return [var("k%d"%j) for j in range(L) if eq.has(var("k%d"%j))]


def integrand_maple(graph_obj):
    """
    :param graph_obj: instance of D_to_infty_graph class
    :return: integrand as a string
    """
    tv = graph_obj.get_time_versions()
    # В integrands соберем все пары (param_int, non_int_expr) со всех вычитаний во всех вр версиях.
    # Этот список полностью определяет диаграмму.
    integrands = []
    for tv_num, ver in enumerate(tv):       ## Loop over time versions
        all_int_of_tv = substration(*integrand_with_ext_mom(graph_obj, tv_num))  ## list of all integrands of certain tv
        integrands += list(map(lambda sub: feyn_repr(sub), all_int_of_tv))
    return integrands

