read "HyperInt.mpl":
hyper_verbosity := 10:
_hyper_check_divergences := false:


diff_vars;
for i in diff_vars do
    f:=-diff(f,i):
    f:=subs(i=0,f);
end do:

f:=-subs(p=1,diff(f,p)):

L:=[seq(u||i,i=1..parse(convert(diff_vars[1],string)[2..])-1)]:

convert(series(f, E = 0, E_order_max), polynom, E):
hyperInt(eval(%, L[1] = 1), L[2..]): fibrationBasis(%, []): f:=collect(%, E):

non_int:=convert(series(non_int, E = 0, E_order_max), polynom, E):
res:=convert(series(simplify(non_int*f), E=0, E_order_max), polynom, E);
fd:=FileTools[Text][Open](convert(file_name, string),append=true):
FileTools[Text][WriteLine](fd, convert(res,string)):
