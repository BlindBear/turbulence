from sympy import *
import sys
sum = 0
loop = int(sys.argv[1])
order = int(sys.argv[2])

with open(f'diags_{loop}_loops/ans/order_{order}/results_{loop}loops_{order}order.txt', 'r') as f:
    data = f.readlines()

zeta2 = symbols('zeta2')
zeta3 = symbols('zeta3')
for diag in data:
    res = diag.split(">")[1].replace(' ','').replace('\n','').replace('zeta[2]','zeta2').replace('zeta[3]','zeta3')
    sum+=sympify(res)

print(simplify(sum))
