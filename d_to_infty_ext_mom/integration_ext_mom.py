#! /usr/bin/ipython
# ! encoding: utf8

__author__ = "kirienko"

import os
import multiprocessing as mp
from os import mkdir, listdir, chdir
from os.path import join as pjoin
from os.path import exists
import time


def maple_calc(diag):
    import os
    #os.chdir(os.path.expanduser('~') + '/PycharmProjects/turb_ext_mom/d_to_infty_ext_mom')
    os.chdir(os.path.expanduser('~') + '/workspace/turbulence/d_to_infty_ext_mom')
    from d_to_infty_class_ext_mom import D_to_infty_graph as D
    #from get_integrands_ext_mom_old import integrand_maple
    from get_integrands_ext_mom_with_subgraph_filter import integrand_maple
    from maple import maple_term, maple_collect
    from config import loops, order, sleep_time

    name = diag.replace('|','i').replace(':','x').replace('A','b')
    feyn_diag = integrand_maple(D(diag))
    open(name, 'w').close()
    for term in feyn_diag:
        print('TERM: ', term)
        time.sleep(sleep_time)
        maple_term(*term, name)
    time.sleep(sleep_time)
    maple_collect(name, loops, order)
    os.remove(name)   #удаляет промежуточные файлы, где расписаны все члены диаграммы


def diags_from_static(loops):
    """
    :param path_to_nonzero: path to files with name `static_diag` which contain dynamic diagrams list
    :return: list of diagrams by listing `nonzero` directory
    """
    #static_diags = os.listdir(path_to_nonzero)
    static_diags = listdir(os.getcwd() + '/diags_%s_loops/nonzero'%loops)
    dyn_diags = []
    for sd in static_diags:
        with open(os.getcwd() + '/diags_%d_loops/nonzero/' % loops + sd) as fd:
            dyn_diags += [dd.strip() for dd in fd.readlines()]
    return dyn_diags


if __name__ == "__main__":
    start_time = time.time()
    try:
        from config import *
    except:
        loops = 3
        order = 0
        digits = 10
        ipython_profile = 'default'

    parallel = True

    #abspath = expanduser('~') + '/PycharmProjects/Graph3_final/d_to_infty_ext_mom'
    #ans_dir = pjoin(abspath, 'diags_%s_loops'%loops, 'ans')
    ans_dir = pjoin(os.getcwd(), 'diags_%s_loops' % loops, 'ans')
    #Создаем папку ints и папку order в ней, если еще нет
    if not exists(ans_dir):
        mkdir(ans_dir)
        print("Created: %s" % ans_dir)
    ans_order_dir = pjoin(ans_dir, 'order_%d' % order)
    if not exists(ans_order_dir):
        mkdir(ans_order_dir)
        print("Created: %s"%ans_order_dir)

    diags = diags_from_static(loops)

    #Удаляет все файлы из папки с ответами (ИСПРАВИТЬ ПО УМНОМУ)
    if False:
        chdir(ans_order_dir)
        files = glob.glob(ans_order_dir)
        for f in files:
            open(f, 'w').close()

    #Parallel
    if parallel:
        with mp.Pool(mp.cpu_count()) as p:
            p.map(maple_calc, diags)
    #No parallel
    else:
        for i, d in enumerate(diags):
            print(i)
            print(d)
            maple_calc(d)
            print('---------------------------------------------------------------------------------------------------')

    #Собирает результаты с отдельных файлов в один
    chdir(ans_order_dir)
    inputs = []
    for file in os.scandir('.'):
        inputs.append(file.name)
    with open('results_%sloops_%sorder.txt'%(loops, order), 'w') as outfile:
        for fname in inputs:
            with open(fname) as infile:
                outfile.write(infile.read())

    print("--- %s seconds ---" % (time.time() - start_time))
