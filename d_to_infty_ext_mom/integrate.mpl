read "HyperInt.mpl":
hyper_verbosity := 10:
_hyper_check_divergences := false:

diff_vars;
for i in diff_vars do
    f:=-diff(f,i):
    f:=subs(i=0,f):
end do:

f:=-subs(p=1,diff(f,p)):

L:=[seq(u||i,i=1..parse(convert(diff_vars[1],string)[2..])-1)]:

coeff(convert(series(simplify(f*non_int), E = 0, E_order+1),polynom,E), E, E_order):
hyperInt(eval(%, L[1] = 1), L[2..]): fibrationBasis(%, []): res:=collect(%, E):

fd:=FileTools[Text][Open](convert(name_to, string),append=true):
FileTools[Text][WriteLine](fd, convert(res,string)):
FileTools[Text][Close](fd):