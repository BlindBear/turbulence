f:=0:
name_from:=convert(name_from, string):
line:=readline(name_from):
while line<>0 do
    f:=f+parse(line):
    line:=readline(name_from):
end do:
f:=collect(f, E):

with(StringTools):
result:=cat(SubstituteAll(SubstituteAll(SubstituteAll(name_from,"i","|"),"x",":"),"b","A"), " -> ", convert(f,string)):
name_to:=SubstituteAll(Split(name_from, "x")[1], "i", "-"):
path:=FormatMessage("diags_%1_loops/ans/order_%2/%3", loops, E_order, name_to):
fd:=FileTools[Text][Open](path,append=true):
FileTools[Text][WriteLine](fd, result):
FileTools[Text][Close](fd):