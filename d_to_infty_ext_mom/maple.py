import os

def maple_term(param_int, non_int_expr, diff_vars, name_to):
    from config import maple_path, order
    os.system(maple_path + ' -t -q -c "f:=%s" -c "non_int:=%s" -c "diff_vars:=%s" -c "E_order:=%s" -c "name_to:=%s" integrate.mpl' % (
        param_int.replace(' ', ''), non_int_expr.replace(' ', ''), diff_vars.replace(' ', ''), order, name_to))


def maple_collect(name_from, loops, order):
    from config import maple_path
    os.system(maple_path + ' -t -q -c "name_from:={}" -c "loops:={}" -c "E_order:={}" collect_terms.mpl'.format(name_from, loops, order))
