#! /usr/bin/python
#! encoding: utf8

__author__ = "kirienko"

import nickel

import os
import sys

import graph_state as gs
import networkx as nx
from copy import deepcopy
import graphine, graph_state_config_with_fields
import itertools as it

#import dynamic_diagram_generator

## Filters
connected = graphine.filters.connected
oneIR = graphine.filters.one_irreducible
@graphine.filters.graph_filter
def self_energy(sub_graph_edges, super_graph):
    """
    Filter for self-energy subgraphs (with 2 external legs)
    """
    if len(sub_graph_edges):
        if len([e for e in sub_graph_edges if e.is_external()])==2:
            return True
    return False

def test(seq,rules):
    for comp in rules:
        if False in [seq.index(x_y[0])<seq.index(x_y[1]) for x_y in comp]:
            return
    return seq

def is_significant_subgraph(sg,tv):
    """
    Returns True if subgraph 'sg' is significant for time version 'tv',
        False otherwise.
    Significance means that all nodes of subgraph appear in the time version
    sequentially, with no breakage (the order of nodes does not matter).
    E.g. for the time version [0,1,2,3,4,5] the subgraph [3,2,4] is significant,
    whereas [2,4,5] is not.
    """
    for i,v in enumerate(tv):
        if v in sg:
            if sorted(tv[i:i+len(sg)]) == sg:
                return True
            else:
                return False

class D_to_infty_graph:
    def __init__(self, nickel_str):
        self.nickel = nickel_str
        self.Loops = graph_state_config_with_fields.from_str(self.nickel).loops_count
        self.D = self.nx_graph_from_str(nickel_str)
        self.U = self.D.to_undirected()
        if self.is_zero():
            print("Diagram has zero value")

        self.spine = self.spine_search()  # !!!!!!!!!!!!!!!!!!!!

    def __str__(self):
        return self.nickel

    def nx_graph_from_str(self, nickel_str):
        """
        Returns networkx MultiGraph object from Nickel string
        NB: here we 'forget' about graph_state ancestry
        """
        if not ":" in str(nickel_str):
            gs_diag = self.static_diag(nickel_str)
            edges = [tuple([n.index for n in e.nodes]) for e in gs_diag.edges]
            ext = [e for e in edges if -1 in e]
            ext_index = lambda x: 1-x.index(-1)
            g = nx.MultiGraph()

            for i,e in enumerate(ext):
                    edges.remove(e)
                    edges.append((e[ext_index(e)],-i-1))
            g.add_edges_from(edges)

        else:
            gs_diag = self.dynamic_diag(nickel_str)
            edges = [e for e  in gs_diag.edges if not e.is_external()]
            ext = [e for e  in gs_diag.edges if e.is_external()]

            g = nx.MultiDiGraph()
            for e in edges:
                i,j = tuple([n.index for n in e.nodes])
                g.add_edge(i,j,fields = e.fields)
            for k,e in enumerate(ext):
                i,j = tuple([n.index for n in e.nodes])
                g.add_edge(-k-1,j,fields = e.fields)
        return g

    @staticmethod
    def static_diag(diag_from_str):
        sc = gs.PropertiesConfig.create()
        return sc.graph_state_from_str(diag_from_str)

    @staticmethod
    def dynamic_diag(diag_from_str):
        sc = gs.PropertiesConfig.create(
                    gs.PropertyKey(name="fields",
                                    is_directed=True,
                                    externalizer=gs.Fields.externalizer()))
        return sc.graph_state_from_str(diag_from_str)

    @staticmethod
    def is_phiphi(fields):
        """
        Returns True if array 'fields' contains
        at least one of ["aa","dd","ad","da"],
        False -- elsewise
        """
        return bool([x for x in fields if x in ["aa","dd","ad","da"]])

    def get_fields(self,ee):
        """
        Returns fields of edges 'ee' in multigraph U.
        'ee' is a tuple (node1, node2)
        """
        #return [str(x.get('fields')) for x in list(self.U.edge[ee[0]][ee[1]].values())]  old
        return [str(x.get('fields')) for x in list(self.U.adj[ee[0]][ee[1]].values())]

    def find_simple_momenta(self):
        """
        Returns pairs (edge,field) where 'edge'
        corresponds to simple momentum
        """
        ans = set()
        counter = 1
        edges = set([(e1,e2) for e1,e2 in self.U.edges()])
        for e1,e2 in edges:
            #print (e1,e2)
            if not (e1<0 or e2<0):
                #fields = [(k,v['fields']) for k,v in list(self.U.edge[e1][e2].items())]  old
                fields = [(k, v['fields']) for k, v in list(self.U.adj[e1][e2].items())]
                for f in fields:
                    if self.is_phiphi([str(f[1])]):
                        ans.add(((e1,e2,f[0]),f))
                        if counter <= self.Loops:
                            self.U.add_edge(e1,e2,f[0],mom=int("0"*counter+"1"+"0"*(self.Loops-counter),2))
                            #print "Add mom","0"*counter+"1"+"0"*(Loops-counter)," at",(e1,e2,f[0])
                        counter += 1
            elif e1<0 or e2<0:
                self.U.add_edge(e1,e2,0,mom=int("1"+"0"*self.Loops,2))
        return list(ans)

    def subgraph_simple_momenta(self,gs_subgraph):
        """
        :param gs_subgraph: graphine subgraph
        :return: list of internal (for this subgraph) momenta, i.g. ['k1','k3']
        """
        internal_nodes = [x for x in gs_subgraph.vertices if x>-1]
        all_simple_momenta = set([m[0] for m in self.find_simple_momenta()])
        subgraph_edges     = set(self.U.subgraph(internal_nodes).edges(keys=True))
        simple_edges       = all_simple_momenta.intersection(subgraph_edges)
        return [self.momenta_in_edge(x)[0] for x in simple_edges]


    def is_zero(self):
        """
        Returns True if diag 'name' has zero value,
        False otherwise.
        """
        ## find simple momenta
        self.find_simple_momenta()

        ## find independent cycles with simple momenta
        self.find_cycles()
        ## copy momenta to undirected graph:
        #[[Z[e1][e2][k]['mom'] for k in Z[e1][e2]] for e1,e2 in Z.edges()]
        for e1,e2 in self.D.edges():
            for k in self.D[e1][e2]:
                self.D.add_edge(e1,e2,k,mom=self.U[e1][e2][k]['mom'])
        
        ## find 'bridges'
        for i in range(len(self.D.nodes())-2):
            self.flow_near_node(i)
        
        self.U = self.D.to_undirected()
        ## test bridges over cycles
        self.bridges = self.find_bridges()
        for b in self.bridges:
            test = [int(self.flow_near_node(x),2) for x in b]
            if not test[0] & test[1]:
                return True
        return False

    def find_cycles(self):
        """
        Black magic explained:
        Here we go through the internal nodes (= all nodes except 2) and
        search for such ones that have TWO [of 3] edges with
        the property 'mom[entum]' defined. Then we can define 'mom' on the third edge
        as mom3 = mom1 XOR mom2
        In the end 'mom' is defined on the every edge.
        """

        # nodp = number of defined properties in each node:
        nodp = [sum([sum(map(len,list(v.values()))) for v in list(self.U[i].values())]) for i in range(len(self.U.nodes)-2)]
        while 5 in nodp:
            ## 5 means that such a node has 2 of 3 edges with 'mom' defined
            cur_node = nodp.index(5)
            #print "Current node:",cur_node
            mom_map = [(k,list(map(len,list(v.values())))) for k,v in list(self.U[cur_node].items())]
            #print "mom_map =",mom_map
            for elem in mom_map:
                if 1 in elem[1] :# 1 means that 'mom' is undefined for such an edge
                    #print "from_node = %d, to_node = %d"%(cur_node,elem[0])
                    #print self.U[cur_node][elem[0]]
                    ## edges with already defined 'mom's
                    m = list(set(self.U[cur_node].keys())-set([elem[0]]))
                    #print "m =",m
                    if len(m) == 2:
                        mom1 = list(self.U[cur_node][m[0]].values())[0]['mom']
                        mom2 = list(self.U[cur_node][m[1]].values())[0]['mom']
                    elif len(m) == 1 and (len(mom_map)>2 or sum(elem[1])>1): # <-- 'mom' is already defined at
                        m = list(self.U[cur_node].keys())                       # one of two edges between 'from-to' nodes
                        #print "new m =",m
                        line_num_1 = [k for k,v in list(self.U[cur_node][m[0]].items()) if 'mom' in list(v.keys())][0]
                        line_num_2 = [k for k,v in list(self.U[cur_node][m[1]].items()) if 'mom' in list(v.keys())][0]
                        mom1 = list(self.U[cur_node][m[0]].values())[line_num_1]['mom']
                        mom2 = list(self.U[cur_node][m[1]].values())[line_num_2]['mom']
                    elif len(m) == 1 and len(mom_map)==2 and sum(elem[1])==1:# <-- 'mom's are already defined at
                              # two edges between the same nodes
                        mom1 = self.U[cur_node][m[0]][0]['mom']
                        mom2 = self.U[cur_node][m[0]][1]['mom']

                    else:
                        raise
                    line_num =  list(map(len,list(self.U[cur_node][elem[0]].values()))).index(1)
                    #print "\tmom1 =",mom1,",\tmom2 =",mom2,",\tmom1^mom2 =",bin(mom1^mom2)
                    self.U.add_edge(cur_node,elem[0],line_num,mom=mom1^mom2)
                    nodp = [sum([sum(map(len,list(v.values()))) for v in list(self.U[i].values())]) for i in range(len(self.U.nodes)-2)]
        return

    def find_bridges(self):
        """
        Returns pairs of nodes that are connected by bridges:
        [(0,1),(2,3),(4,5)]
        """
        fields = lambda x: self.U[x[0]][x[1]][x[2]]['fields']
        bridges = []
        for i in range(len(self.U.nodes())-2):
            # print "\ni = %d"%i
            cur_edge = (i,self.U.nodes[i]['dot'][0],self.U.nodes[i]['dot'][1])
            # print "Current edge:", cur_edge
            bridges += [[cur_edge]]
            if str(fields(cur_edge)) == 'dd':
                continue
            while True:
                next_node = cur_edge[1]
                #print "next node: %d"%next_node
                next_lines = deepcopy(self.U[next_node])
                #print "next lines:",next_lines

                #next_lines[cur_edge[0]].pop(cur_edge[2])  old
                #Чтобы избежать бага, надо все перевести из AtlasView в словари
                next_lines = dict(next_lines)
                for i in next_lines:
                    next_lines[i] = dict(next_lines[i])

                next_lines[cur_edge[0]].pop(cur_edge[2])

                dot = self.U.nodes[next_node]['dot']
                next_lines[dot[0]].pop(dot[1])
                for (k,v) in list(next_lines.items()):
                    if len(v) == 0:
                        next_lines.pop(k)
                #print "next lines:",next_lines
                nce1 = list(next_lines.keys())[0]
                nce2 = list(next_lines[nce1].keys())[0]
                cur_edge = (next_node,nce1,nce2)
                bridges[-1] += [cur_edge]
                if 'd' in str(fields(cur_edge)):
                    break
        bridge_set = set()
        # print "bridges:", bridges
        for b in bridges:
            bridge_set.add(tuple(sorted([b[0][0],b[-1][1]])))
        return list(bridge_set)

    def flow_near_node(self,node):
        """
        Returns the number of simple momenta that flows through the node
        by the dotted edge (binary string like '0100').
        For directed graphs only.
        """
        mom = []
        pr = self.D.predecessors(node)
        for n in pr:
            for nn in self.D[n][node]:
                if 'd' == self.D[n][node][nn]['fields'][-1]:
                    self.D.add_node(node,dot = (n,nn))
                    continue
                mom += [self.D[n][node][nn]['mom']]
        to_nodes = self.D.successors(node)
        for n in to_nodes:
            for nn in self.D[node][n]:
                if 'd' == self.D[node][n][nn]['fields'][0]:
                    self.D.add_node(node,dot = (n,nn))
                    continue
                mom += [self.D[node][n][nn]['mom']]

        return bin(mom[0] & mom[1])[2:]

    def momenta_near_node(self,node):
        """
        The same as 'flow_near_node', but returns the list of momenta.
        """
        binary_mom = self.flow_near_node(node)
        return ["k%d"%j for j,k in enumerate(binary_mom[::-1]) if int(k) and j < self.Loops]


    #!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    #ПОИСК ХРЕБТА
    def spine_search(self):
        G = graph_state_config_with_fields.from_str(self.nickel)
        edges = G.edges()
        ext_nodes = tuple(gs.operations_lib.get_bound_vertices(G))

        edges = [e for e in edges if not e.is_external()]
        aa_edges = []
        for edge in edges:
            if 'd' not in edge.fields:
                aa_edges.append(edge)

        connected_parts = gs.operations_lib.get_connected_components(aa_edges)
        for part in connected_parts:
            if ext_nodes[0] in part and ext_nodes[1] in part:
                spine_nodes = part
                break

        spine = []
        for edge in aa_edges:
            if edge.nodes[0] in spine_nodes and edge.nodes[1] in spine_nodes:
                spine.append(edge)
        return spine


    def momenta_in_edge_with_ext_momentum(self, e):
        """
        УЧЕТ ВНЕШНЕГО ИМПУЛЬСА
        e = (n1, n2, n3) tuple
        Перебираем линии из хребта и ищем совпадение с линией e.
        Дополнительно проверяем, нет ли точки (d) на линии е.
        Если оба условия выполнены, то дописываем импульс p.
        """
        mom = self.U[e[0]][e[1]][e[2]]['mom']
        field = self.U[e[0]][e[1]][e[2]]['fields']
        momenta_list = ["k%d"%j for j,k in enumerate(bin(mom)[:1:-1]) if int(k) and j < self.Loops]

        for gs_edge in self.spine:
            if set(e[:2]) == set(gs_edge.nodes) and 'd' not in str(field):
                momenta_list.append('p')

        return momenta_list
    # !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


    def momenta_in_edge(self,e):
        """
        :param G: networkx MultiGraph
        :param edge: (e0,e1,e2)
        :return: the list of momenta, i.e. ['k0','k2','k3']
        """
        mom = self.U[e[0]][e[1]][e[2]]['mom']
        return ["k%d"%j for j,k in enumerate(bin(mom)[:1:-1]) if int(k) and j < self.Loops]
    
    def get_time_versions(self):
        """
        Returns all time versions of a graph G with Nickel representation 'nickel_str',
            along with all significant subgraphs that correspond every time version:
        >>> z = D_to_infty_graph("e12|e3|34|5|55||:0A_dd_aA|0a_Aa|dd_aA|Aa|aA_dd||")
        >>> z.get_time_versions()
        [([0, 2, 4, 5, 3, 1], [e11|e|:0A_aA_dd|0a|, e12|e3|33||:0A_dd_aA|0a_Aa|aA_dd||])]

        Also creates self.tv = self.get_time_versions()
        Directed edges in our diagram form directed acyclic graph (DAG) with a root 'root'.
        
        """
        T =  nx.DiGraph()
        for e1 in self.U.adj:
            for e2 in self.D.adj[e1]:
                for k in self.D[e1][e2]:
                    if 'A' in self.D[e1][e2][k]['fields']:
                        idx = str(self.D[e1][e2][k]['fields']).index('A')
                        from_node = (e1,e2)[1-idx]
                        to_node   = (e1,e2)[idx] 
                        T.add_edge(from_node,to_node)
        ## Find all self-energy subgraphs
        G = graph_state_config_with_fields.from_str(self.nickel)
        subgraphs = [x for x in G.x_relevant_sub_graphs(filters=connected+oneIR+self_energy)]
        sg_nodes  = [sorted([v for v in sg.vertices if v>0]) for sg in subgraphs]
    
        ## Find the root of DAG:
        root   = [i for i in T.nodes if T.in_degree(i) == 0][0]
        if root < 0:
            T.remove_node(root)
            root = [i for i in T.nodes if T.in_degree(i) == 0][0]
        ## Nodes of DAG:
        nodes = list(set(T.nodes())-set([root]))
        ## Leaves of DAG:
        leaves = [l for l in T.nodes() if T.out_degree(l)==0]   
        ## Find all paths from root to every leaf:
        paths = []
        for i in leaves:
            paths += [i for i in nx.algorithms.all_simple_paths(T,root,i)]
        ## Comparison rules for every two nodes that define whether
        ##      time version is correct: 
        test_cmp = [[i for i in it.combinations(p,2)] for p in paths]
        ## Find all possible permutations of nodes   
        perm = [[root]+list(i) for i in it.permutations(nodes,len(nodes))]
        ## Apply the rules of comparison
        vers = []
        for p in perm:
            ver = test(p,test_cmp)
            if ver:
                ## Test if subgraph is significant for this time version
                vers += [(ver,\
                    [subgraphs[i] for i, sg in enumerate(sg_nodes) if
                        is_significant_subgraph(sg,ver)
                     and '0A' in str(subgraphs[i])
                     and '0a' in str(subgraphs[i])])]
        self.tv = vers

        #Отброс временных версий, где линии вне базы!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        if False:
            #Поиск никелевого индекса вершины,
            #к которой цепляется второй внешний хвост
            nx_diag = self.D  # мультиграф из networkx
            if self.nickel.split(':')[1][0:2] == '0A':
                for edge in nx_diag.edges:
                    if edge[0] == -2:
                        end_index = edge[1]
                        #print(end_index)
            elif self.nickel.split(':')[1][0:2] == '0a':
                end_index = 0
            else:
                raise Exception('not 0A or 0a')

            #Перебор вр. версий и отсев тех,
            #где линии вылазят за базу(хребет)
            vers = list(filter(lambda tv: tv[0][-1] == end_index, self.tv))
            self.tv = vers

        return vers


    def visualize_pdf(self):
        try:
            import graphviz
        except ImportError:
            print("you need to install graphviz module for python")
            sys.exit(1)

        def prepare_static(nomenkl):
            nomenkl = nomenkl.split(':')[0]

            def isInternal(node):
                return node != -1

            edges = nickel.Nickel(string=nomenkl).edges

            lines = []
            nodes = dict()
            ext_cnt = 0

            for edge in edges:
                nodes__ = list()
                for node in edge:
                    if isInternal(node):
                        nodes__.append(("%s_%s" % (nomenkl, node), "%s" % node))
                    else:
                        nodes__.append(("%s_E_%s" % (nomenkl, ext_cnt), "ext"))
                        ext_cnt += 1
                    if nodes__[-1][0] not in nodes.keys():
                        nodes[nodes__[-1][0]] = nodes__[-1][1]
                lines.append([n[0] for n in nodes__])

            return nodes, lines

        def prepare_dynamic(nomenkl):
            dyn_nickel = nomenkl.split(':')[1]  # обрезаем динамические указатели
            dyn_nickel = dyn_nickel.split('|')  # отделяем указатели для разных узлов
            dyn_nickel = list(
                map(lambda s: s.split('_'), dyn_nickel))  # внешний список - по узлам, внутренний - по линиям из узла

            # В динамической нотации необходимо поменять местами указатели линий, идущих на внешние узлы - см. как устроен lines
            for i in range(len(dyn_nickel)):
                for j in range(len(dyn_nickel[i])):
                    if '0' in dyn_nickel[i][j]:
                        dyn_nickel[i][j] = dyn_nickel[i][j][::-1]

            dyn_nickel = list(it.chain.from_iterable(
                dyn_nickel))  # развернули все вложенные списки в один большой (можно, т.к. все линии по порядку)

            return dyn_nickel

        def graph_struct(nomenkl):
            nodes, lines = prepare_static(nomenkl)
            dyn_nickel = prepare_dynamic(nomenkl)

            graph = graphviz.Digraph(name=nomenkl)
            graph.graph_attr['splines'] = 'spline'
            graph.graph_attr['margin'] = '1.5'
            graph.graph_attr['nodesep'] = '0.33'
            graph.graph_attr['layout'] = 'neato'

            for node in nodes:
                if nodes[node] == 'ext':
                    graph.node(str(node), shape='point', style='invis')
                else:
                    graph.node(str(node), shape='point')

            # Расшифровка динамической нотации
            notation_to_graph = {'a': 'none', '0': 'none', 'A': 'nonenonenonenonenonenonetee',
                                 'd': 'nonenonenonenonenonenonedot'}

            count = 0

            for line in lines:
                tail = notation_to_graph[dyn_nickel[count][0]]
                head = notation_to_graph[dyn_nickel[count][1]]

                if 'E' in line[0] or 'E' in line[1]:
                    graph.edge(line[0], line[1], dir='both', arrowtail=tail, arrowhead=head, arrowsize='0.5', len='0.3')
                else:
                    graph.edge(line[0], line[1], dir='both', arrowtail=tail, arrowhead=head, arrowsize='0.5', )

                count += 1

            return graph

        def save_pdf(nomenkl, filename=None):
            if filename == None:
                filename = nomenkl.replace('|', '!').replace(':', '$')
                graph = graph_struct(nomenkl)
                graph.render(filename)

            return filename

        res = save_pdf(self.nickel)
        print("File %s created in %s" % (res, os.getcwd()))