from d_to_infty_class_ext_mom import D_to_infty_graph as D
from sympy import var, factor, ln, prod, simplify, integrate, diff
from copy import deepcopy
from sign_account import sign_account
from collections import defaultdict
import os, sys
import time
from functools import reduce

#e12|e3|34|5|55||:0A_dd_aA|0a_Aa|dd_aA|Aa|aA_dd|| # 3 loop 1 tv основная
#e12|e3|33||:0A_dd_aA|0a_Aa|aA_dd|| # 1 tv
#e12|34|35|4|5|e|:0A_aA_da|da_aA|dA_Ad|dd|aA|0a| # 2 bridges, one of them of 2 edges
#e12|e3|34|5|55||:0A_aA_dA|0a_da|dd_aA|Aa|aA_dd|| 2 subgraphs, no stretches
#e12|e3|34|5|55||:0A_aA_dA|0a_da|dd_aA|Aa|aA_dd|| - расписана у ЛЦ
#e12|23|4|45|6|e7|77||:0a_da_Aa|dd_Aa|Aa|dd_Ad|Aa|0A_aA|Aa_dd|| - 4loop, 21 tv
#e12|e3|33||:0A_dd_aA|0a_Aa|aA_dd|| - 2loop 1 tv (красивая)
#e12|e3|33||:0A_aA_da|0a_dA|Aa_dd|| - 2loop 1 tv

from config import maple_path
from get_integrands_ext_mom_with_subgraph_filter import integrand_maple
import graph_state as gs
import graphine, graph_state_config_with_fields

diag = D('e12|e3|34|5|55||:0A_aA_dA|0a_dA|dd_aa|aA|Aa_dd||')
print(diag.get_time_versions())
E_order_max = 3  # макс порядок разложения
#diag.visualize_pdf()

start_time = time.time()
feyn_diag = integrand_maple(diag) #список из кортежей, каждый из которых соотв одной подстановке
file_name = diag.nickel.replace('|','i').replace(':','x')
print(file_name)
open(file_name, 'w').close()
for term in feyn_diag:
    print('TERM: ', term)
    param_int = term[0].replace(' ', '')
    non_int_expr = term[1].replace(' ', '')
    diff_vars = term[2].replace(' ', '')
    os.system(maple_path + ' -t -q -c "f:={}" -c "non_int:={}" -c "diff_vars:={}" -c "E_order_max:={}" -c "file_name:={}" integrate_single_diag.mpl'.format(
        param_int, non_int_expr, diff_vars, E_order_max, file_name))
os.system(maple_path + ' -t -q -c "file_name:={}" collect_terms_single_diag.mpl'.format(file_name))

print("--- %s seconds ---" % (time.time() - start_time))


